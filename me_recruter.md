# Me recruter

Si vous êtes arrivés ici, soit vous avez été poussé par votre curiosité, soit vous songez à me recruter !

J'ai décider de réaliser ce document afin de favoriser grandement le matching de poste, tant sur des aspects techniques, mais aussi sur des éléments de valeurs, de softs skills

## Qui suis-je en quelques mots 🧑‍💻

### Ce que je voudrais faire

J'ai pour objectif de mettre ma **passion** pour le **dev** au service de la **préservation** de l'**environnement** 🌿

Je souhaite également œuvrer pour plus de **justice sociale** 🤝

Et aider les avancées des découvertes **scientifiques** 🧑‍🔬

### Comment j'aime faire

J'essaie d'**accélérer** les **boucles de feedback** ↗️➿

Je préfère faire **moins mais mieux** que de faire mal plein de choses en même temps 📉

J'ai principalement travaillé sur des solutions web

J'aime co-créer les fonctionnalités de manière bout en bout : de l'UX, en passant par le front-end (SEO, A11y …), le back-end (API, DB …), la façon de les amener jusqu'à la production (CI / CD)

```mermaid
flowchart LR
    dev[Équipes de devs]
    tech_lead[Tech Lead]
    livrable[Livrable]
    vision[Vision]

    dev ==>|créent| livrable
    tech_lead -->|responsable des aspects technique| livrable
    tech_lead -->|¤| dev

    %% triche pour aligner
    livrable ~~~ vision
    livrable ~~~ vision
    livrable -.-> vision
    livrable ~~~ vision
    livrable ~~~ vision

    tech_lead -.-> vision

    classDef vision stroke-dasharray: 3 5;
    class vision vision
```

¤ le tech lead par rapport aux équipes de devs :

* Organise un contexte où il y est :
  * **facile** de faire de **bonnes** choses
  * **difficile** de faire de **mauvaises** choses
* Défini la vision technique
* Aide à concrétiser la vision technique
* Crée une ambiance favorable à l'apprentissage et donc le droit à l'erreur
* Pousse vers l'autonomie des équipes
* Incite à la coopération, l'entraide, le partage de connaissances...

### Historique 🗓️

* 2008 : découverte du dev
* 2012 : premier travail en tant que dev en alternance
* 2018 : premier CDI en tant que dev

Pour le détail de l'historique, vous pouvez aller voir [mon CV](https://bruno_ebstein.gitlab.io/cv/cv_bruno_ebstein.html)

## Mes valeurs clés

* **Impact** de l'entreprise / du projet
  * Rendre le monde moins pire
* Ouverture aux pratiques **[craft][Craft Manifesto]**
  * S'améliorer en continu
* **Communauté**
  * Grandir ensemble : via le partage, l'entraide, l'intelligence collective …
* **Bienveillance**
  * Prendre soin des humains avec qui on travaille
* **Transparence**
  * Avoir accès aux informations permet de faire de meilleurs choix
* **Horizontalité** et gouvernance distribuée 🗳️ [^horizontalité]
  * Travailler de manière participative

[^horizontalité]: IMHO, les décisions TOP-DOWN ⬇️🕴️ sont souvent déconnectées des difficultés de la réalité, elles sont généralement décontextualisées ne laissant pas d'autres choix et empèche d'avoir les meilleurs résultats

## Mes domaines de prédilection technique

Je suis assez **ouvert** sur les technologies, car se sont des outils, des moyens pour apporter de la valeur

Le langage que je connais le mieux est **TypeScript**

J'aime beaucoup **Rust** :

* le langage est
  * prévu pour etre testé sans avoir besoin d'outils externes
  * verbeux mais tout y est très explicite
  * strict : quand ça compile, il y a peu de risque d'avoir des erreurs techniques seulement des erreurs logiques / métiers de programmation
* le compilateur essaie d'aider l'utilisateur plutot que lui afficher un message d'erreur difficilement compréhensible comme le font certains langages
* son écosystème est
  * bien pensé
  * mature dans ses choix
  * robuste

J'aime les langages ayant un **paradigme fonctionnel** surtout lorsqu'ils sont fortement typés (Elm, Haskell …) : ça permet de concentrer ses efforts sur la résolution du problème plutot que de créer des couches virtuelles de modélisations qui ne sont pas exprimées par le métier

J'aime aussi beaucoup **simplifier** les tâches de développement :

* en créant des **outils**
* en **automatisant** certaines tâches (script, CI / CD)
* avoir des **environnements reproductibles** (Nix, container …)

Je m'intéresse beaucoup à la **qualité logicielle**, qui permet d'avoir un logiciel :

* _fonctionnel_ (évidemment)
* _maintenable_ (donc testé)
* _évoluable_

## Ce que je peux vous apporter

Immédiatement : facilitation de sessions de **mob programming**

Dès les premières semaines :

* un **point de vue extérieur**, sur l'organisation, les méthodes, les outils, le code …
* **mise en place**, **évolution** et **maintenance** de projets

Dans les premiers mois,
en ayant construit une relation de confiance avec l'équipe
avec le temps :

* **mentoring** des juniors
* animation de **formations** interne
  * git
  * tests
  * CI / CD
  * …

## Ce que j'adore

Faire du **mob programming**, du **pair programming**

Découvrir de nouvelles **méthodes**, de nouvelles **pratiques** qui permettent d'améliorer notre façon de travailler et le résultat de notre travail

## Ce que j'aimerais grandement découvrir

Un cadre mature sur les **bonnes pratiques** de développement

Une volonté d'aller vers du **mob programming à plein temps** (ou presque)

Un cadre d'**apprentissage**, d'entraide, de partage où il est question de grandir collectivement

Une entreprise qui travaille avec la [méthode FaST](https://www.fastagile.io/method)

## Mon espace évasion

### Veille

Je participe à quelques meetups et conférences

## Mes principaux critères de sélections et pourquoi

| Mes critères principaux                              | Les explications                                                                      |
|:-----------------------------------------------------|:--------------------------------------------------------------------------------------|
| **Impact** de l'entreprise / du projet               | Rendre le monde moins pire                                                            |
| Ouverture aux pratiques **[craft][Craft Manifesto]** | S'améliorer en continu                                                                |
| [Agile][Agile Manifesto]                             | Avoir vraiment compris l'état d'esprit qui va avec [^agile]                           |
| **Communauté**                                       | Grandir ensemble : via le partage, l'entraide, l'intelligence collective …            |
| **Bienveillance**                                    | Prendre soin des humains avec qui on travaille                                        |
| **Horizontalité** et **Transparence**                | Avoir accès aux informations pour faire les meilleurs choix                           |
| **Télétravail**                                      | Calme, comfort, souplesse des horaires, moins de transport ↘️🚇, plus de sommeil ↗️😴 |
| Ouvert au 4/5                                        | Meilleurs équilibre vie pro 🆚 vie perso ⚖️                                           |
| Bonne **ambiance** et **feeling**                    | Nous pouvons faire des choses sérieuses tout en nous amusant [^mauvais_entretien]     |

[^agile]: ne pas se contenter de faire n'importe quoi en ayant une gestion de projet catastrophique et prétendre être Agile

[^mauvais_entretien]: Si pendant les entretiens, mes interviewers essaient de me mettre mal à l'aise pendant toute la durée, j'imagine que c'est une entreprise dans laquelle j'aurais du mal à me sentir en sécurité psychologique et donc faire du travail de qualité

---

En lisant tout cela, j'espère que vous en saurez beaucoup plus sur moi. Et si vous pensez qu'il y a un matching entre nous, je serai ravi de discuter avec vous ! 📩

[ebstein.bruno+job@gmail.com](mailto:ebstein.bruno+job@gmail.com)

[LinkedIn](https://fr.linkedin.com/in/brunoebstein)

[Craft Manifesto]: http://manifesto.softwarecraftsmanship.org/#/fr-fr

[Agile Manifesto]: https://agilemanifesto.org/iso/fr/manifesto.html
